
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Задание 4</title>
</head>

<body>

    <?php
	    if (!empty($result_message)) {
	        print('<div id="messages">');
	        foreach ($result_message as $message) {
		        print($message);
	        }
	        print('</div>');
	      }
    ?>

    <div class="main">
    <div class="block_4">
        <h1>Форма</h1>
        <form method="POST" id="form" action="index.php">
            <label>Имя:<br />
                <input type=text name="field-name" placeholder="Введите ваше имя" 
                <?php if($errors['field-name']){print 'class = "error"';}?> 
                value="<?php print $values['field-name']; ?>" />
            </label><br />

            <label>E-mail:<br />
                <input name="field-email" placeholder="Введите ваш e-mail" type="email"
                <?php if($errors['field-email']){print 'class = "error"';}?>
                value="<?php print $values['field-email']; ?>">
            </label><br />

            <label>Дата рождения:<br />
            <input type="date" name="field-date" <?php if($errors['field-date']){print 'class = "error"';}?>  value="<?php print $values['field-date']; ?>">
            </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-sex" value="M" <?php if ($values['radio-sex'] == "M") {print 'checked';} ?> />Мужской
            </label>
            <label class="radio"><input type="radio" name="radio-sex" value="F" <?php if ($values['radio-sex'] == "F") {print 'checked';} ?>/>Женский
            </label><br />

            <label>Выберите кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-limb" value=0 <?php if ($values['radio-limb'] == '0') {print 'checked';} ?>/>0
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=1 <?php if ($values['radio-limb'] == '1') {print 'checked';} ?>/>1
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=2 <?php if ($values['radio-limb'] == '2') {print 'checked';} ?>/>2
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=3 <?php if ($values['radio-limb'] == '3') {print 'checked';} ?>/>3
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=4 <?php if ($values['radio-limb'] == '4') {print 'checked';} ?>/>4
            </label><br />

            <label>Ваши сверхспособности:<br />
                <select multiple="true" name="superpower[]" <?php if($errors['superpower']){print 'class = "error"';}?> >
                    <option value="1" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Бессмертие</option>
                    <option value="2" <?php if ($values['superpower']['1']) {print 'selected';} ?>>Прохождение сквозь стены</option>
                    <option value="3" <?php if ($values['superpower']['2']) {print 'selected';} ?>>Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="BIO" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label <?php if ($errors['ch']) {print 'class="error"';} ?>>
                <input name="ch" type="checkbox" checked=checked value=1>С контрактом ознакомлен:<br />
            </label>

            <input type="submit" value="Отправить" />
        </form>
    </div>
</div>
</div>
   
</body>

</html>

